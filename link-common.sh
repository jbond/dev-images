#!/bin/sh

# Handle files for buster and stretch base images:
for file in common/base/*; do
  for destination_dir in stretch buster; do
    basename="$(basename "$file")"
    link="dockerfiles/$destination_dir/$basename"
    if [ -L "$link" ]; then
      echo "$link exists"
    else
      ln -s "../../$file" "$link"
    fi
  done
done

# Handle files for jobrunner images:
for file in common/jobrunner/*; do
  for destination_dir in dockerfiles/stretch-*-jobrunner dockerfiles/buster-*-jobrunner; do
    basename="$(basename "$file")"
    link="$destination_dir/$basename"
    if [ -L "$link" ]; then
      echo "$link exists"
    else
      ln -s "../../$file" "$link"
    fi
  done
done
