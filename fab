#!/bin/bash
set -eu -o pipefail

# Configure your .ssh/config so it can access the following hosts with proper username and key:
# - contint.wikimedia.org
# - contint1001.wikimedia.org
# - contint2001.wikimedia.org

_ssh_command_sudo() {
    local host="$1"
    local sudoer="$2"
    local cmds

    # strip new lines and leading space
    cmds=$(echo "$3" | sed 's/^ *//' | sed '/^$/d')

    if [[ "${4:-visible}" == "visible" ]]; then
        # shellcheck disable=SC2001
        echo "$cmds" | sed "s/^/\\[$host\\] /"
    fi
    ssh "$host" sudo -H -u "$sudoer" "$cmds"
}

_ssh_command_self() {
    local host="$1"
    local cmds
    local esc_cmds

    # strip new lines and leading space
    cmds=$(echo "$2" | sed 's/^ *//' | sed '/^$/d')
    esc_cmds=$(printf '%q' "$cmds")

    # shellcheck disable=SC2001
    echo "$cmds" | sed "s/^/\\[$host\\] /"
    ssh "$host" sh -c "$esc_cmds"
}

_confirm() {
    read -rp "$1 [Y/n] " response
    test "$response" == "" || test "$response" == "y" || test "$response" == "Y"
}

_update_integration_config() {
    local host="$1"
    local git_dir="$2"
    local diff_dir="$3"
    local log_msg="$4"

    _ssh_command_self "$host" "git -C \"$git_dir\" remote update"
    _ssh_command_self "$host" "git -C \"$git_dir\" --no-pager log -p HEAD..origin/main \"$diff_dir\""

    _confirm "Does the diff look good?"
    _confirm "Did you log a message for the SAL in #wikimedia-releng (e.g. \"$log_msg\")"

    _ssh_command_self "$host" "git -C \"$git_dir\" rebase"
    _ssh_command_self "$host" "git -C \"$git_dir\" -c gc.auto=128 gc --auto --quiet" "invisible"
}

# Update docker-pkg built development images.
deploy_devimages() {
    local host='contint.wikimedia.org'
    local git_dir='/srv/dev-images'
    local diff_dir='dockerfiles/'
    local log_msg='!log Updating development images on contint primary for [url]'

    _update_integration_config "$host" "$git_dir" "$diff_dir" "$log_msg"

    # docker-pkg does not attempt to pull images - T219398
    _ssh_command_self "$host" '
    docker pull docker-registry.wikimedia.org/buster
    docker pull docker-registry.wikimedia.org/bullseye
    '

    local docker_pkg='/srv/deployment/docker-pkg/venv/bin/docker-pkg'
    local docker_pkg_config='/etc/docker-pkg/dev-images.yaml'
    local dockerfiles='/srv/dev-images/dockerfiles'
    _ssh_command_self "$host" "
    cd /tmp
    $docker_pkg -c $docker_pkg_config --info build $dockerfiles
    cat /tmp/docker-pkg-build.log
    "

    _confirm "Delete build log?"
    _ssh_command_self "$host" 'rm /tmp/docker-pkg-build.log'
}

case "${1:-}" in
    deploy_devimages) deploy_devimages ;;
    *)
    echo "usage: ./fab deploy_devimages"
    exit 1
    ;;
esac
