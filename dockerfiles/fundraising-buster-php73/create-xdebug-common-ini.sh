#!/bin/bash

# Dynamically create config for xdebug, so we can include the current IP
# address of the host machine as seen from the container (since Docker does not
# make this easily discoverable using DNS).

host_ip=$(route -n | grep 'UG[ \t]' | awk '{print $2}')

cat << EOF > /srv/config/internal/xdebug-common.ini
xdebug.remote_log=/tmp/xdebug.log
xdebug.remote_host=${host_ip}
xdebug.remote_enable=on
EOF
