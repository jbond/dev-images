FROM {{ "fundraising-buster-php73" | image_tag }} AS fundraising-buster-php73

{% set packages|replace('\n', ' ') -%}
apache2
libapache2-mod-php
{%- endset -%}

RUN {{ packages | apt_install }}

COPY ./ports.conf /etc/apache2/ports.conf
COPY ./envvars /etc/apache2/envvars
COPY ./000-default.conf /etc/apache2/sites-available/

RUN a2enmod ssl
RUN service apache2 restart
RUN a2dissite default-ssl.conf

# xdebug-common.ini will be written by entrypoint.sh every time the container starts, to set
# xdebug.remote_host to the current host IP.
# entrypoint.sh will also create links to xdebug config files on the host.
# Set up for permissions and links needed for all that.
RUN chmod a+rwx /etc/php/7.3/cli/conf.d/ \
    && chmod a+rwx /etc/php/7.3/apache2/conf.d/ \
    && ln -s /srv/config/internal/xdebug-common.ini /etc/php/7.3/cli/conf.d/25-common-xdebug.ini \
    && ln -s /srv/config/internal/xdebug-common.ini /etc/php/7.3/apache2/conf.d/25-common-xdebug.ini

# Default values for some environment variables used in entrypoint script
ENV FR_DOCKER_SERVICE_NAME=civiproxy

EXPOSE 9001

COPY entrypoint.sh /srv/entrypoint.sh

ENTRYPOINT ["/bin/bash", "/srv/entrypoint.sh"]